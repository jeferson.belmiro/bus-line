import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IBus } from '../models/bus';
import { ICoordinate } from '../models/coordinate';

const urlBase = 'http://www.poatransporte.com.br/php/facades/process.php';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  public getBusList(): Observable<IBus[]> {
    return this.http.get<IBus[]>(`${urlBase}?a=nc&p=%25&t=o`);
  }

  public getMiniBusList(): Observable<IBus[]> {
    return this.http.get<IBus[]>(`${urlBase}?a=nc&p=%25&t=l`);
  }

  public getItinerary(bus: IBus): Observable<ICoordinate[]> {
    return this.http.get<ICoordinate[]>(`${urlBase}?a=il&p=` + bus.id);
  }
}
