import { Component, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize } from 'rxjs/operators';
import { IBus } from './models/bus';
import { ICoordinate } from './models/coordinate';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  items: IBus[] = [];
  loading = false;
  filter = new FormControl();
  items$ = new BehaviorSubject<IBus[]>([]);
  modalData: {
    bus: IBus;
    loading: boolean;
    itineraries: ICoordinate[];
  };

  constructor(private api: ApiService, private modal: NgbModal) {
    const requests = [this.api.getBusList(), this.api.getMiniBusList()];

    this.loading = true;
    forkJoin(requests)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe(([bus, minibus]) => {
        this.items = [...bus, ...minibus];
        this.items$.next(this.items);
      });

    this.filter.valueChanges
      .pipe(distinctUntilChanged(), debounceTime(500))
      .subscribe((text: string) => {
        const filtered = this.items.filter((bus) => {
          const term = text.toLowerCase();
          return (
            bus.nome.toLowerCase().includes(term) || bus.codigo.includes(term)
          );
        });
        this.items$.next(filtered);
      });
  }

  public showItinerary(bus: IBus, template: TemplateRef<IBus>) {
    this.modalData = {
      bus,
      itineraries: [],
      loading: true,
    };
    this.api
      .getItinerary(bus)
      .pipe(finalize(() => (this.modalData.loading = false)))
      .subscribe((response) => {
        this.modalData.itineraries = Object.keys(response).map((key) => {
          if (!isNaN(+key)) {
            return response[key];
          }
        });
      });
    this.modal.open(template, { scrollable: true });
  }

  public openCoordinate(item: ICoordinate) {
    const url = `https://www.google.com/maps/?q=${item.lat},${item.lng}`;
    window.open(url, '_blank');
  }
}
